// function that takes in an object and the id to append to
function createListFromObject(obj, elementIdString) {
  // Loops over the job.essentials object, creating new li elements for each value
  $.each(obj, function(key, val) {
    let li = $('<li>');

    if (typeof val === 'number' && val.toString().length > 7) {
      val = new Date(val).toString();
      console.log(val);
    }

    if (val === 'Good') {
      console.log(val);
    }

    // Checks for nested objects
    if (typeof val === 'object' && val.length === undefined) {
      // console.log(key, val);
      li
        .addClass('clickable-row hand')
        .text(`${key}: `)
        .attr('id', key)
        .appendTo(elementIdString)
      ;
      // Recursively call the function to create elements from the nested object's values
      createListFromObject(val, elementIdString);
    } else {
      li
        .addClass('clickable-row hand')
        .text(`${key}: ${val}`)
        .attr('id', key)
        .appendTo(elementIdString)
      ;
    }
  });
};

// jQuery scripts

$(document).ready(function() {

  $('#headline').text(job.headline);

  createListFromObject(job.essentials, '#essentials-list');

  createListFromObject(job.methodology, '#methodology-list');

  createListFromObject(job.specs, '#specs-list');

  createListFromObject(job.profile, '#profile-list');

  createListFromObject(job.equipment, '#equipment-list');

  createListFromObject(job.technologies, '#technologies-list');

  createListFromObject(job.bonuspoints, '#bonuspoints-list');

  createListFromObject(job.other, '#other-list');

  createListFromObject(job.misc, '#misc-list');

  $('h3').addClass('animated fadeInLeftBig');

  // Line animation helper that reveals one li at a time
  var delay = 1000;

  $('li').each(function() {
    $(this).hide().delay(delay).fadeIn(1250);
    delay += 1000;
  });

});
